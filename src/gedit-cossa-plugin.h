/*
 * gedit plugin for GTK+ CSS
 *
 * ©2011 Carlos Garnacho <carlosg@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GEDIT_COSSA_PLUGIN_H__
#define __GEDIT_COSSA_PLUGIN_H__

#include <glib.h>
#include <glib-object.h>
#include <libpeas/peas-extension-base.h>
#include <libpeas/peas-object-module.h>

G_BEGIN_DECLS

#define COSSA_TYPE_PLUGIN         (cossa_plugin_get_type ())
#define COSSA_PLUGIN(o)	          (G_TYPE_CHECK_INSTANCE_CAST ((o), COSSA_TYPE_PLUGIN, CossaPlugin))
#define COSSA_PLUGIN_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), COSSA_TYPE_PLUGIN, CossaPluginClass))
#define COSSA_IS_PLUGIN(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), COSSA_TYPE_PLUGIN))
#define COSSA_IS_PLUGIN_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), COSSA_TYPE_PLUGIN))
#define COSSA_PLUGIN_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), COSSA_TYPE_PLUGIN, CossaPluginClass))

typedef struct _CossaPlugin CossaPlugin;
typedef struct _CossaPluginClass CossaPluginClass;
typedef struct _CossaPluginPrivate CossaPluginPrivate;

struct _CossaPlugin
{
  PeasExtensionBase parent_instance;

  CossaPluginPrivate *priv;
};

struct _CossaPluginClass
{
  PeasExtensionBaseClass parent_class;
};

GType                cossa_plugin_get_type (void) G_GNUC_CONST;
G_MODULE_EXPORT void peas_register_types   (PeasObjectModule *module);

G_END_DECLS

#endif /* __COSSA_PLUGIN_H__ */
