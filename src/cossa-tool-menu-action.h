/*
 * gedit plugin for GTK+ CSS
 *
 * ©2011 Carlos Garnacho <carlosg@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __COSSA_TOOL_MENU_ACTION_H__
#define __COSSA_TOOL_MENU_ACTION_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define COSSA_TYPE_TOOL_MENU_ACTION         (cossa_tool_menu_action_get_type ())
#define COSSA_TOOL_MENU_ACTION(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), COSSA_TYPE_TOOL_MENU_ACTION, CossaToolMenuAction))
#define COSSA_TOOL_MENU_ACTION_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), COSSA_TYPE_TOOL_MENU_ACTION, CossaToolMenuActionClass))
#define COSSA_IS_TOOL_MENU_ACTION(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), COSSA_TYPE_TOOL_MENU_ACTION))
#define COSSA_IS_TOOL_MENU_ACTION_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), COSSA_TYPE_TOOL_MENU_ACTION))
#define COSSA_TOOL_MENU_ACTION_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), COSSA_TYPE_TOOL_MENU_ACTION, CossaToolMenuActionClass))

typedef struct _CossaToolMenuAction CossaToolMenuAction;
typedef struct _CossaToolMenuActionClass CossaToolMenuActionClass;

struct _CossaToolMenuAction
{
  GtkAction parent_instance;
  gpointer priv;
};

struct _CossaToolMenuActionClass
{
  GtkActionClass parent_class;
};

GType            cossa_tool_menu_action_get_type      (void) G_GNUC_CONST;
GtkAction *      cossa_tool_menu_action_new           (void);


G_END_DECLS

#endif /* __COSSA_TOOL_MENU_ACTION_H__ */
