/*
 * gedit plugin for GTK+ CSS
 *
 * ©2011 Carlos Garnacho <carlosg@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>

#ifndef __COSSA_PREVIEWER_H__
#define __COSSA_PREVIEWER_H__

#include <gtk/gtk.h>
#include "cossa-style-provider.h"

G_BEGIN_DECLS

#define COSSA_TYPE_PREVIEWER         (cossa_previewer_get_type ())
#define COSSA_PREVIEWER(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), COSSA_TYPE_PREVIEWER, CossaPreviewer))
#define COSSA_PREVIEWER_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST    ((c), COSSA_TYPE_PREVIEWER, CossaPreviewerClass))
#define COSSA_IS_PREVIEWER(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), COSSA_TYPE_PREVIEWER))
#define COSSA_IS_PREVIEWER_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE    ((c), COSSA_TYPE_PREVIEWER))
#define COSSA_PREVIEWER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS  ((o), COSSA_TYPE_PREVIEWER, CossaPreviewerClass))

typedef struct CossaPreviewer CossaPreviewer;
typedef struct CossaPreviewerClass CossaPreviewerClass;

struct CossaPreviewer
{
  GtkWidget parent_instance;
  gpointer priv;
};

struct CossaPreviewerClass
{
  GtkWidgetClass parent_class;
};

typedef enum {
  COSSA_ZOOM_1_1 = 1,
  COSSA_ZOOM_2_1 = 2,
  COSSA_ZOOM_4_1 = 4
} CossaZoomLevel;

GType       cossa_previewer_get_type              (void) G_GNUC_CONST;

GtkWidget * cossa_previewer_new                   (void);
void        cossa_previewer_add_sample            (CossaPreviewer *previewer,
                                                   GtkWidget      *sample);
void        cossa_previewer_select_sample         (CossaPreviewer *previewer,
                                                   GtkWidget      *sample);

void           cossa_previewer_set_zoom_level     (CossaPreviewer *previewer,
                                                   CossaZoomLevel  zoom);
CossaZoomLevel cossa_previewer_get_zoom_level     (CossaPreviewer *previewer);

void           cossa_previewer_update_samples     (CossaPreviewer *previewer);

GtkCssProvider * cossa_previewer_get_style        (CossaPreviewer *previewer);

G_END_DECLS

#endif /* __COSSA_PREVIEWER_H__ */
