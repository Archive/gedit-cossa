/*
 * Cossa standalone applicaton
 *
 * ©2011 Carlos Garnacho <carlosg@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __COSSA_APPLICATION_H__
#define __COSSA_APPLICATION_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define COSSA_TYPE_APPLICATION         (cossa_application_get_type ())
#define COSSA_APPLICATION(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), COSSA_TYPE_APPLICATION, CossaApplication))
#define COSSA_APPLICATION_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), COSSA_TYPE_APPLICATION, CossaApplicationClass))
#define COSSA_IS_APPLICATION(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), COSSA_TYPE_APPLICATION))
#define COSSA_IS_APPLICATION_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), COSSA_TYPE_APPLICATION))
#define COSSA_APPLICATION_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), COSSA_TYPE_APPLICATION, CossaApplicationClass))

typedef struct _CossaApplication CossaApplication;
typedef struct _CossaApplicationClass CossaApplicationClass;

struct _CossaApplication
{
  GtkApplication parent_instance;
  gpointer priv;
};

struct _CossaApplicationClass
{
  GtkApplicationClass parent_class;
};

GType            cossa_application_get_type      (void) G_GNUC_CONST;

GApplication *   cossa_application_new           (void);


G_END_DECLS

#endif /* __COSSA_APPLICATION_H__ */
