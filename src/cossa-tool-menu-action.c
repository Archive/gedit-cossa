/*
 * gedit plugin for GTK+ CSS
 *
 * ©2011 Carlos Garnacho <carlosg@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "cossa-tool-menu-action.h"

G_DEFINE_TYPE (CossaToolMenuAction, cossa_tool_menu_action, GTK_TYPE_ACTION)

static void
cossa_tool_menu_action_init (CossaToolMenuAction *action)
{
}

static void
cossa_tool_menu_action_class_init (CossaToolMenuActionClass *klass)
{
  GTK_ACTION_CLASS (klass)->toolbar_item_type = GTK_TYPE_MENU_TOOL_BUTTON;
}

GtkAction *
cossa_tool_menu_action_new (void)
{
  return g_object_new (COSSA_TYPE_TOOL_MENU_ACTION, NULL);
}
