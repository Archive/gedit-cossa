/*
 * gedit plugin for GTK+ CSS
 *
 * ©2011 Carlos Garnacho <carlosg@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __COSSA_WINDOW_H__
#define __COSSA_WINDOW_H__

#include "cossa-previewer.h"
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define COSSA_TYPE_WINDOW         (cossa_window_get_type ())
#define COSSA_WINDOW(o)	          (G_TYPE_CHECK_INSTANCE_CAST ((o), COSSA_TYPE_WINDOW, CossaWindow))
#define COSSA_WINDOW_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), COSSA_TYPE_WINDOW, CossaWindowClass))
#define COSSA_IS_WINDOW(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), COSSA_TYPE_WINDOW))
#define COSSA_IS_WINDOW_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), COSSA_TYPE_WINDOW))
#define COSSA_WINDOW_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), COSSA_TYPE_WINDOW, CossaWindowClass))

typedef struct _CossaWindow CossaWindow;
typedef struct _CossaWindowClass CossaWindowClass;

struct _CossaWindow
{
  GtkWindow parent_instance;
  gpointer priv;
};

struct _CossaWindowClass
{
  GtkWindowClass parent_class;

  void (* update) (CossaWindow *window);
};

GType            cossa_window_get_type      (void) G_GNUC_CONST;

GtkWidget *      cossa_window_new           (void);
CossaPreviewer * cossa_window_get_previewer (CossaWindow *window);


G_END_DECLS

#endif /* __COSSA_WINDOW_H__ */
