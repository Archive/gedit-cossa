/*
 * gedit plugin for GTK+ CSS
 *
 * ©2011 Carlos Garnacho <carlosg@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include "gedit-cossa-plugin.h"
#include "cossa-window.h"

#include <glib/gi18n-lib.h>
#include <gedit/gedit-debug.h>
#include <gedit/gedit-window.h>
#include <gedit/gedit-window-activatable.h>
#include <gedit/gedit-view.h>
#include <gedit/gedit-view-activatable.h>

#define MENU_PATH "/MenuBar/ToolsMenu/ToolsOps_5"
#define COSSA_WINDOW_PREVIEW "GeditCossaPluginWindowPreview"

enum {
  PROP_0,
  PROP_WINDOW,
  PROP_VIEW
};

static void gedit_window_activatable_iface_init (GeditWindowActivatableInterface *iface);
static void gedit_view_activatable_iface_init (GeditViewActivatableInterface *iface);

static void preview_activated_cb                (GtkAction       *action,
                                                 CossaPlugin     *plugin);

G_DEFINE_DYNAMIC_TYPE_EXTENDED (CossaPlugin,
				cossa_plugin,
				PEAS_TYPE_EXTENSION_BASE,
				0,
				G_IMPLEMENT_INTERFACE_DYNAMIC (GEDIT_TYPE_WINDOW_ACTIVATABLE,
							       gedit_window_activatable_iface_init)
				G_IMPLEMENT_INTERFACE_DYNAMIC (GEDIT_TYPE_VIEW_ACTIVATABLE,
							       gedit_view_activatable_iface_init))

struct _CossaPluginPrivate
{
  GeditWindow *window;
  GeditView *view;
  GtkTextTag *error_tag;
  GtkTextTag *warning_tag;

  GtkActionGroup *ui_action_group;
  guint ui_id;
};

static const GtkActionEntry action_entries[] = {
  { "Preview", GTK_STOCK_SORT_ASCENDING, N_("_Preview theme"),
    "<Ctrl>F8", N_("Preview GTK+ theme"), G_CALLBACK (preview_activated_cb) }
};


static void
cossa_plugin_init (CossaPlugin *plugin)
{
  gedit_debug_message (DEBUG_PLUGINS, "CossaPlugin initializing");
  plugin->priv = G_TYPE_INSTANCE_GET_PRIVATE (plugin,
                                              COSSA_TYPE_PLUGIN,
                                              CossaPluginPrivate);
}

static void
cossa_plugin_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  CossaPluginPrivate *priv = COSSA_PLUGIN (object)->priv;

  switch (prop_id)
    {
    case PROP_WINDOW:
      priv->window = g_value_dup_object (value);
      break;
    case PROP_VIEW:
      priv->view = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
cossa_plugin_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  CossaPluginPrivate *priv = COSSA_PLUGIN (object)->priv;

  switch (prop_id)
    {
    case PROP_WINDOW:
      g_value_set_object (value, priv->window);
      break;
    case PROP_VIEW:
      g_value_set_object (value, priv->view);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
cossa_plugin_dispose (GObject *object)
{
  CossaPluginPrivate *priv = COSSA_PLUGIN (object)->priv;

  if (priv->window != NULL)
    {
      g_object_unref (priv->window);
      priv->window = NULL;
    }

  if (priv->view != NULL)
    {
      g_object_unref (priv->view);
      priv->view = NULL;
    }

  if (priv->ui_action_group != NULL)
    {
      g_object_unref (priv->ui_action_group);
      priv->ui_action_group = NULL;
    }

  G_OBJECT_CLASS (cossa_plugin_parent_class)->dispose (object);
}

static void
cossa_plugin_class_init (CossaPluginClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = cossa_plugin_set_property;
  object_class->get_property = cossa_plugin_get_property;
  object_class->dispose = cossa_plugin_dispose;

  g_object_class_override_property (object_class, PROP_WINDOW, "window");
  g_object_class_override_property (object_class, PROP_VIEW, "view");

  g_type_class_add_private (klass, sizeof (CossaPluginPrivate));
}

static void
cossa_plugin_class_finalize (CossaPluginClass *klass)
{
}

static void
cossa_window_activatable_activate (GeditWindowActivatable *activatable)
{
  CossaPluginPrivate *priv;
  GtkUIManager *manager;

  gedit_debug (DEBUG_PLUGINS);

  priv = COSSA_PLUGIN (activatable)->priv;
  manager = gedit_window_get_ui_manager (priv->window);

  priv->ui_action_group = gtk_action_group_new ("GeditCossaPluginActions");
  gtk_action_group_set_translation_domain (priv->ui_action_group,
                                           GETTEXT_PACKAGE);

  gtk_action_group_add_actions (priv->ui_action_group,
                                action_entries,
                                G_N_ELEMENTS (action_entries),
                                activatable);

  gtk_ui_manager_insert_action_group (manager,
                                      priv->ui_action_group,
                                      -1);

  priv->ui_id = gtk_ui_manager_new_merge_id (manager);

  gtk_ui_manager_add_ui (manager, priv->ui_id,
                         MENU_PATH, "Preview", "Preview",
                         GTK_UI_MANAGER_MENUITEM,
                         FALSE);
}

static void
cossa_window_activatable_deactivate (GeditWindowActivatable *activatable)
{
  CossaPluginPrivate *priv;
  GtkUIManager *manager;

  gedit_debug (DEBUG_PLUGINS);

  priv = COSSA_PLUGIN (activatable)->priv;
  manager = gedit_window_get_ui_manager (priv->window);

  gtk_ui_manager_remove_ui (manager, priv->ui_id);
  gtk_ui_manager_remove_action_group (manager, priv->ui_action_group);
}

static void
gedit_window_activatable_iface_init (GeditWindowActivatableInterface *iface)
{
  iface->activate = cossa_window_activatable_activate;
  iface->deactivate = cossa_window_activatable_deactivate;
}

static gchar *
get_view_css (GeditView *view)
{
  GtkTextBuffer *buffer;
  GtkTextIter start, end;

  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
  gtk_text_buffer_get_bounds (buffer, &start, &end);

  return gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
}

static void
update_style (CossaWindow *window,
              GeditView   *view)
{
  GtkTextBuffer *doc;
  GtkTextIter start, end;
  CossaPreviewer *previewer;
  GtkCssProvider *provider;
  gchar *style_css;

  style_css = get_view_css (view);
  previewer = cossa_window_get_previewer (window);
  provider = cossa_previewer_get_style (previewer);
  doc = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
  gtk_text_buffer_get_bounds (doc, &start, &end);

  /* remove previous applied tags */
  gtk_text_buffer_remove_tag_by_name (doc, "cossa-error-tag", &start, &end);
  gtk_text_buffer_remove_tag_by_name (doc, "cossa-warning-tag", &start, &end);

  if (gtk_css_provider_load_from_data (provider, style_css, -1, NULL))
    cossa_previewer_update_samples (previewer);

  g_free (style_css);
}

static void
on_parsing_error (GtkCssProvider *provider,
                  GtkCssSection  *section,
                  GError         *error,
                  GtkTextView    *view)
{
  GtkTextBuffer *buffer;
  GtkTextIter start, end;
  const gchar *tag_name;

  buffer = gtk_text_view_get_buffer (view);

  gtk_text_buffer_get_iter_at_line_index (buffer,
                                          &start,
                                          gtk_css_section_get_start_line (section),
                                          gtk_css_section_get_start_position (section));
  gtk_text_buffer_get_iter_at_line_index (buffer,
                                          &end,
                                          gtk_css_section_get_end_line (section),
                                          gtk_css_section_get_end_position (section));

  if (g_error_matches (error, GTK_CSS_PROVIDER_ERROR, GTK_CSS_PROVIDER_ERROR_DEPRECATED))
    tag_name = "cossa-warning-tag";
  else
    tag_name = "cossa-error-tag";

  gtk_text_buffer_apply_tag_by_name (buffer, tag_name, &start, &end);
}

static void
on_document_saved (GeditDocument *doc,
                   const GError  *error,
                   CossaPlugin   *plugin)
{
  if (error == NULL)
    {
      GtkWidget *window;
      CossaPluginPrivate *priv;

      priv = plugin->priv;
      window = g_object_get_data (G_OBJECT (priv->view), COSSA_WINDOW_PREVIEW);

      if (window != NULL)
        update_style (COSSA_WINDOW (window), priv->view);
    }
}

static void
cossa_view_activatable_activate (GeditViewActivatable *activatable)
{
  CossaPluginPrivate *priv;
  GeditDocument *doc;

  gedit_debug (DEBUG_PLUGINS);

  priv = COSSA_PLUGIN (activatable)->priv;

  doc = GEDIT_DOCUMENT (gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->view)));

  priv->error_tag = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (doc),
                                                "cossa-error-tag",
                                                "underline-set", TRUE,
                                                "underline", PANGO_UNDERLINE_ERROR,
                                                NULL);
  priv->warning_tag = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (doc),
                                                  "cossa-warning-tag",
                                                  "underline-set", TRUE,
                                                  "underline", PANGO_UNDERLINE_ERROR,
                                                  "foreground-set", TRUE,
                                                  "foreground", "orange",
                                                  NULL);

  g_signal_connect (doc, "saved",
                    G_CALLBACK (on_document_saved),
                    activatable);
}

static void
cossa_view_activatable_deactivate (GeditViewActivatable *activatable)
{
  CossaPluginPrivate *priv;
  GeditDocument *doc;
  GtkWidget *window;
  GtkTextTagTable *tag_table;

  gedit_debug (DEBUG_PLUGINS);

  priv = COSSA_PLUGIN (activatable)->priv;

  doc = GEDIT_DOCUMENT (gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->view)));
  tag_table = gtk_text_buffer_get_tag_table (GTK_TEXT_BUFFER (doc));

  gtk_text_tag_table_remove (tag_table, priv->error_tag);
  gtk_text_tag_table_remove (tag_table, priv->warning_tag);

  g_signal_handlers_disconnect_by_func (doc, on_document_saved, activatable);

  window = g_object_get_data (G_OBJECT (priv->view), COSSA_WINDOW_PREVIEW);

  if (window != NULL)
    {
      gtk_widget_destroy (window);
      g_object_set_data (G_OBJECT (priv->view), COSSA_WINDOW_PREVIEW, NULL);
    }
}

static void
gedit_view_activatable_iface_init (GeditViewActivatableInterface *iface)
{
  iface->activate = cossa_view_activatable_activate;
  iface->deactivate = cossa_view_activatable_deactivate;
}

static void
preview_activated_cb (GtkAction   *action,
                      CossaPlugin *plugin)
{
  CossaPluginPrivate *priv;
  GeditView *cur_view;
  GtkWidget *window;

  gedit_debug (DEBUG_PLUGINS);

  priv = COSSA_PLUGIN (plugin)->priv;
  cur_view = gedit_window_get_active_view (priv->window);
  window = g_object_get_data (G_OBJECT (cur_view), COSSA_WINDOW_PREVIEW);

  if (!window)
    {
      GeditTab *cur_tab;
      gchar *tab_name;
      gchar *title;
      CossaPreviewer *previewer;
      GtkCssProvider *provider;

      window = cossa_window_new ();
      g_signal_connect (window, "delete-event",
                        G_CALLBACK (gtk_widget_hide_on_delete),
                        NULL);

      g_signal_connect (window, "update",
                        G_CALLBACK (update_style),
                        cur_view);

      previewer = cossa_window_get_previewer (COSSA_WINDOW (window));
      provider = cossa_previewer_get_style (previewer);
      g_signal_connect (provider, "parsing-error",
                        G_CALLBACK (on_parsing_error),
                        cur_view);

      cur_tab = gedit_window_get_active_tab (priv->window);

      tab_name = _gedit_tab_get_name (cur_tab);

      /* Translators: the %s is refered to the name of the document */
      title = g_strdup_printf (_("GTK+ Theme preview - %s"),
                               tab_name);
      g_free (tab_name);
      gtk_window_set_title (GTK_WINDOW (window), title);
      g_free (title);

      gtk_window_set_default_size (GTK_WINDOW (window), 400, 400);

      g_object_set_data (G_OBJECT (cur_view), COSSA_WINDOW_PREVIEW, window);
    }

  gtk_widget_show (window);
  update_style (COSSA_WINDOW (window), cur_view);
}

G_MODULE_EXPORT void
peas_register_types (PeasObjectModule *module)
{
  cossa_plugin_register_type (G_TYPE_MODULE (module));

  peas_object_module_register_extension_type (module,
                                              GEDIT_TYPE_WINDOW_ACTIVATABLE,
                                              COSSA_TYPE_PLUGIN);
  peas_object_module_register_extension_type (module,
                                              GEDIT_TYPE_VIEW_ACTIVATABLE,
                                              COSSA_TYPE_PLUGIN);
}
