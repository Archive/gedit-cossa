/*
 * Cossa standalone applicaton
 *
 * ©2011 Carlos Garnacho <carlosg@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "cossa-application.h"
#include <gtk/gtk.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
  GApplication *application;
  GError *error = NULL;
  int status;

  g_type_init ();
  application = cossa_application_new ();

  g_application_register (application, NULL, &error);

  if (error)
    {
      g_warning ("Could not initialize application: %s", error->message);
      g_error_free (error);
      return EXIT_FAILURE;
    }

  if (argc < 2)
    {
      g_printerr ("USAGE - %s <FILE>\n", argv[0]);
      return EXIT_FAILURE;
    }

  status = g_application_run (G_APPLICATION (application), argc, argv);
  g_object_unref (application);

  return status;
}
