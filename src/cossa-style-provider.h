/*
 * gedit plugin for GTK+ CSS
 *
 * ©2011 Carlos Garnacho <carlosg@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __COSSA_STYLE_PROVIDER_H__
#define __COSSA_STYLE_PROVIDER_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define COSSA_TYPE_STYLE_PROVIDER         (cossa_style_provider_get_type ())
#define COSSA_STYLE_PROVIDER(o)	          (G_TYPE_CHECK_INSTANCE_CAST ((o), COSSA_TYPE_STYLE_PROVIDER, CossaStyleProvider))
#define COSSA_STYLE_PROVIDER_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), COSSA_TYPE_STYLE_PROVIDER, CossaStyleProviderClass))
#define COSSA_IS_STYLE_PROVIDER(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), COSSA_TYPE_STYLE_PROVIDER))
#define COSSA_IS_STYLE_PROVIDER_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), COSSA_TYPE_STYLE_PROVIDER))
#define COSSA_STYLE_PROVIDER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), COSSA_TYPE_STYLE_PROVIDER, CossaStyleProviderClass))

typedef struct _CossaStyleProvider CossaStyleProvider;
typedef struct _CossaStyleProviderClass CossaStyleProviderClass;

struct _CossaStyleProvider
{
  GtkCssProvider parent_instance;
  gpointer priv;
};

struct _CossaStyleProviderClass
{
  GtkCssProviderClass parent_class;
};

GType              cossa_style_provider_get_type         (void) G_GNUC_CONST;

GtkStyleProvider * cossa_style_provider_new              (void);
void               cossa_style_provider_set_use_fallback (CossaStyleProvider *provider,
                                                          gboolean            use_fallback);
gboolean           cossa_style_provider_get_use_fallback (CossaStyleProvider *provider);

G_END_DECLS

#endif /* __COSSA_WINDOW_H__ */
